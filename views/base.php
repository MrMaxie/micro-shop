<!DOCTYPE html>
<html>
	<head>
		<meta charset='utf-8'>
		<title>MicroShop</title>
		<!--<link rel='stylesheet' href='/fonts/fonts.css'>-->
		<link rel='stylesheet' href='/css/main.css'>
	</head>
	<body>
		<header>
			<?php if(User::isLogged()): ?>
				<div class='hello'>
					Witaj, 
					<strong><?= User::getLogged()->login ?></strong>
				</div>
			<?php endif ?>
			<div class='right'>
				<?php if(!User::isLogged()): ?>
					<a href='/login'>Zaloguj się</a><!--
				--><a href='/register' class='accent'>Zarejestruj się</a>
				<?php else: ?>
					<a href='/logout'>Wyloguj</a>
				<?php endif ?>
			</div>
		</header>
		<aside>
			<a href='/' id='logo'>MicroShop</a>
			<a href='/' class='<?=($page??null)=='home'?'active':null?>'>
				Strona Główna
			</a>
			<a href='/'>Active</a>
			<footer>
				Copyright &copy; <?= date('Y') ?> MicroShop<br>Maciej (Maxie) Mieńko
			</footer>
		</aside>
		<main>
			<section><?= $this->render() ?></section>
		</main>
	</body>
</html>