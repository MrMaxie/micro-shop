<div id='register'>
	<?= $this->renderFile('flashMessages') ?>
	<div class='centered'>
		<form method='POST'>
			<h1>Rejestracja</h1>
			<label>
				Nazwa Użytkownika
				<input type='text' name='login' value='<?= $form_login ?? null ?>'>
			</label>
			<label>
				Adres e-mail
				<input type='email' name='mail' value='<?= $form_mail ?? null ?>'> 
			</label>
			<label>
				Hasło
				<input type='password' name='password'>
			</label>
			<label>
				Powtórz hasło
				<input type='password' name='repassword'>
			</label>
			<label class='captcha'>
				CAPTCHA (tylko jasne znaki są prawidłowe)
				<div class='captcha-flex'>
					<div class='captcha-img'></div>
					<input name='captcha' type='text'>
				</div>
			</label>
			<div class='bottom'>
				<button>Zarejestruj się</button>
			</div>
		</form>
	</div>
</div>