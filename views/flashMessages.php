<?php foreach($this->flushMessages() as $msg): ?>
	<div class='flash-message is-<?= $msg['type'] ?>'>
		<h1><?= ([
			'success' => 'Sukces',
			'error'   => 'Niepowodzenie'
		])[$msg['type']] ?? '---' ?></h1>
		<div class='desc'><?= $msg['message'] ?></div>
	</div>
<?php endforeach ?>