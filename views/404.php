<div id='e404'>
	<h1>404</h1>
	<div class='desc'>
		Wskazana strona nie istnieje<br>lub nie masz uprawnień, aby ją zobaczyć
	</div>
	<div class='btns'>
		<a href='/'>Wróć do strony głównej</a>
	</div>
	<div class='bg'>404</div>
</div>