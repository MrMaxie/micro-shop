<div id='login'>
	<?= $this->renderFile('flashMessages') ?>
	<div class='centered'>
		<form method='POST'>
			<h1>Logowanie</h1>
			<label>
				Nazwa Użytkownika
				<input type='text' name='login' value='<?= $form_login ?? null ?>'>
			</label>
			<label>
				Hasło
				<input type='password' name='password'>
			</label>
			<div class='bottom'>
				<div class='checkbox'>
					<input name='remember' type='checkbox' id='form-remember' <?php if($form_remember??false): ?>checked='checked'<?php endif ?>>
					<label class='for-checkbox' for='form-remember'>
						Zapamiętaj mnie
					</label>
				</div>
				<button>Zaloguj się</button>
			</div>
		</form>
	</div>
</div>