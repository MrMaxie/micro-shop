const gulp    = require('gulp')
const less    = require('gulp-less')
const gutil   = require('gulp-util')
const prefix  = require('gulp-autoprefixer')
const combine = require('stream-combiner2')
const clip    = require('gulp-clip-empty-files')

gulp.task('less', function() {
	let combined = combine.obj([
		gulp.src('./public/less/main.less'),
		less(),
		clip(),
		prefix({
			browsers: ['last 100 versions'],
			flexbox: true,
			remove: false
		}),
		gulp.dest('./public/css')
	])
	combined.on('error', function(e) {
		gutil.log(
			'['+gutil.colors.red('Error')+']',
			e.filename,
			gutil.colors.magenta('->'),
			e.line
		)
		combined.emit('end')
	})
	return combined
});

gulp.task('watch', function() {
	gulp.watch('./public/less/**/*.less', ['less']).on('error', 
		e => console.log(e)
	)
})
