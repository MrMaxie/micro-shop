const http = require('http')
const fs   = require('fs')
let games  = ['239430','3830','214770','322500','290340','42910','238460','391540','310060','289130','241600','207170','72850']
let result = []
let done   = 0

for(let id of games) {
	let game = {
		id: id
	}
	http.get('http://store.steampowered.com/api/appdetails?appids='+id, res => {
		let body = ''
		res.on('data', chunk => {
			body += chunk.toString()
		})
		res.on('end', () => {
			let data = JSON.parse(body)[id]['data']
			http.get(data['header_image'], res => {
				res.pipe(fs.createWriteStream('./public/img/cover/'+id+'.jpg'))
			})
			game.name    = data['name']
			console.log(id)
			game.price   = data['price_overview']['final']
			game.windows = data['platforms']['windows']
			game.mac     = data['platforms']['mac']
			game.linux   = data['platforms']['linux']
			game.genres  = []
			for(let genre of data['genres'])
				game.genres.push(genre['description'])
			result.push(game)
			done++
			if(done === games.length) {
				fs.writeFile('./games.json', JSON.stringify(result), 'utf-8', err => {
					if(err) console.log(err)
				})
			}
		})
	})
}