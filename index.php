<?php
/**
 * @author Maciej (Maxie) Mieńko
 * @package micro-shop
 */
session_start();

/**
 * Constants
 */
define('DS', DIRECTORY_SEPARATOR);
define('ROOT',rtrim(__DIR__, DS));

/**
 * Autoloader
 */
spl_autoload_register(function($classname){
	foreach(['lib', 'models', 'controllers'] as $dir) {
		$filename = ROOT.DS.$dir.DS.str_replace('/\\', DS, $classname).'.php';
		if(file_exists($filename) && include($filename))
			return true;
	}
	return false;
});

new \MicroShop\DB([
	'host' => 'localhost',
	'user' => 'root',
	'pass' => 'usbw',
	'name' => 'micro-shop',
	'port' => 3306
]);

include ROOT.DS.'router.php';