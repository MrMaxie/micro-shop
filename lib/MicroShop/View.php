<?php
namespace MicroShop;

use \MicroShop\Singleton;

class View extends Singleton {
	static $instance;
	/**
	 * Layers of context to render
	 * @var array
	 */
	protected $layers = [];
	/**
	 * Directory of views
	 * @var string
	 */
	protected $dir = ROOT.DS.'views';
	/**
	 * All of data given for rendered layers
	 * @var array
	 */
	protected $data = [];
	/**
	 * Set-up views directory
	 * @param string $dir
	 */
	function __construct(string $dir=null) {
		parent::__construct();
		if($dir !== null)
			$this->dir = $dir;
	}
	/**
	 * Render next layer
	 * @param  array|null $layers
	 * @return string
	 */
	function render($layers=null): string {
		if(is_array($layers))
			$this->layers = $layers;
		if(count($this->layers) < 1)
			return '';
		return $this->renderFile(array_shift($this->layers));
	}
	/**
	 * Set new value of given path
	 * @param string $name  Path with dot-notation
	 * @param mixed  $value
	 */
	function set(string $name, $value) {
		$this->data[$name] = $value;
	}
	/**
	 * Get value of given path
	 * @param  string $name
	 * @return mixed
	 */
	function get(string $name) {
		return $this->data[$name] ?? null;
	}
	/**
	 * Push new flash message
	 * @param  string $type
	 * @param  string $message
	 */
	function pushFlashMessage(string $type, string $message) {
		if(in_array('flashMessages', $_SESSION))
			$_SESSION['flashMessages'] = [];
		if(in_array($type, $_SESSION))
			$_SESSION['flashMessages'][$type] = [];
		$_SESSION['flashMessages'][$type][] = $message;
	}
	/**
	 * Alias of 'pushFlashMessage' for 'error' type
	 * @param string $message
	 */
	function pushError(string $message) {
		$this->pushFlashMessage('error', $message);
	}
	/**
	 * Alias of 'pushFlashMessage' for 'success' type
	 * @param string $message
	 */
	function pushSuccess(string $message) {
		$this->pushFlashMessage('success', $message);
	}
	/**
	 * Flush flash messages
	 * @return array
	 */
	function flushMessages() {
		$messages = $_SESSION['flashMessages'] ?? [];
		$result   = [];
		unset($_SESSION['flashMessages']);
		foreach($messages as $type => $typeMessages) {
			foreach($typeMessages as $message) {
				$result[] = [
					'type'    => $type,
					'message' => $message
				];
			}
		}
		return $result;
	}
	/**
	 * Render selected file without changing queue
	 * @param  string $file
	 * @return string
	 */
	function renderFile(string $file) {
		$file = $this->dir.DS.str_replace('/\\',DS,$file).'.php';
		if(!file_exists($file))
			return '';
		extract($this->data, EXTR_PREFIX_SAME, '__');
		ob_start();
		include($file);
		return ob_get_clean();
	}
}