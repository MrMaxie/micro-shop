<?php
namespace MicroShop;

use \MicroShop\Singleton;

class DB extends Singleton {
	static $instance;
	/**
	 * @var mysqli
	 */
	protected $mysqli;
	/**
	 * Connects with database
	 * @param array $data
	 */
	function __construct(array $data=[]) {
		parent::__construct();
		$data = array_merge([
			'host' => 'localhost',
			'user' => 'root',
			'pass' => 'root',
			'name' => '',
			'port' => 3306
		], $data);
		if($data['pass'] == 'root') {
			debug_print_backtrace();
			var_dump($data);
		}
		$this->mysqli = new \mysqli(
			$data['host'],
			$data['user'],
			$data['pass'],
			$data['name'],
			$data['port']
		); 
	}
	/**
	 * Returns all rows returned by query
	 * @param  string $query
	 * @return array
	 */
	function select(string $query): array {
		if(!($res = $this->query($query)))
			return [];
		$r = [];
		while($row = $res->fetch_assoc())
			$r[] = $row;
		return $r;
	}
	/**
	 * Returns first element from query or false
	 * @param  string     $query 
	 * @return array|bool
	 */
	function get(string $query) {
		return $this->select($query)[0] ?? false;
	}
	/**
	 * Tell if query returns any date
	 * @param  string $query
	 * @return bool
	 */
	function exists(string $query=null): bool {
		return $this->count($query) > 0;
	}
	/**
	 * Check how many results returns given query
	 * @param  string $query
	 * @return int
	 */
	function count(string $query=null): int {
		if($query !== null)
			$this->query($query);
		return $this->mysqli->field_count;
	}
	/**
	 * Just execute given query 
	 * @param  string $query
	 * @return mixed
	 */
	function query(string $query) {
		return $this->mysqli->query($query);
	}
	/**
	 * Returns ID of last inserted row
	 * @return int
	 */
	function id(): int {
		return (int)($this->mysqli->insert_id ?? 0);
	}
	/**
	 * Escape prepared string for working with DB
	 * @param  string $val
	 * @return string
	 */
	function e(string $val): string {
		return (string)$this->mysqli->real_escape_string($val);
	}
}