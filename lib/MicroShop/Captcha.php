<?php
namespace MicroShop;

class Captcha {
	/**
	 * Create captcha image
	 */
	function __construct() {
		$this->captcha = static::randomString(4);
		$_SESSION['captcha'] = $this->captcha;
		$this->salt    = static::randomString(4);
	}
	/**
	 * Print generated captcha
	 */
	function print() {
		header('Content-Type: image/png');
		$img  = imagecreatetruecolor(150, 50);
		$imgBg = imagecolorallocate($img, 255, 255, 255);
		$imgC1 = imagecolorallocate($img, 85 , 168, 253);
		$imgC2 = imagecolorallocate($img, 18 , 30 , 62);
		$font  = ROOT.DS.'public'.DS.'fonts'.DS.'ROMANTIC.TTF';

		$c = str_split($this->captcha);
		$s = str_split($this->salt);

		imagefilledrectangle($img,0,0,150,50,$imgBg);
		$letters = count($c)+count($s);
		for($i=0;$i<$letters;$i++) {
			$char  = '';
			$color = '';
			if(count($c)>0&&count($s)>0) {
				if(rand(0,1)===1) {
					$char  = array_shift($c);
					$color = $imgC1;
				} else {
					$char  = array_shift($s);
					$color = $imgC2;
				}
			} elseif (count($c)>0) {
				$char  = array_shift($c);
				$color = $imgC1;
			} elseif (count($s)>0) {
				$char  = array_shift($s);
				$color = $imgC2;
			}
			imagettftext($img,25,0,(16*$i)+10,35,$color,$font,$char);
		}
		imagepng($img);
		imagedestroy($img);
	}
	/**
	 * Check if captcha is correct
	 * @param  string $captcha
	 * @return bool
	 */
	static function test(string $captcha) {
		$s = $_SESSION['captcha'] ?? false;
		unset($_SESSION['captcha']);
		return $s == trim($captcha);
	}
	/**
	 * Generate random string
	 * @param  int|integer $count
	 * @return string
	 */
	static function randomString(int $count): string {
		$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charsLength = strlen($chars);
    $result = '';
		for ($i = 0; $i < $count; $i++)
			$result .= $chars[rand(0, $charsLength - 1)];
    return $result;
	}
}