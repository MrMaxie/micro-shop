<?php
namespace MicroShop;

use \MicroShop\Model;

class ORM extends Model {
	/**
	 * Name of table used in all of queries
	 * @var string
	 */
	static $table;
	/**
	 * ID of mirrored row
	 * @var 
	 */
	protected $id;
	/**
	 * Create new row or connect to existing
	 * @param array|int $data
	 */
	function __construct($data) {
		parent::__construct();
		if(is_array($data)) {
			// Creating new row in database
			$this->db->query(sprintf(
				'INSERT INTO `%s` (`%s`) VALUES (\'%s\')',
				static::$table,
				join('`, `',array_keys($data)),
				join('\', \'', array_map(function($x){
					return $this->db->e($x);
				}, $data))
			));
			$this->id = $this->db->id();
		} elseif (is_numeric($data)) {
			// Create mirror object for given ID
			$this->id = (int)$data;
		} else throw new TypeError('ORM object cannot be created with other types than array (for create row) and numeric (for connect to existing row)');
	}
	/**
	 * Returns given value from mirrored row
	 * @param  string $name
	 * @return mixed
	 */
	function __get($name) {
		return $this->db->get(sprintf(
			'SELECT `%s` FROM `%s` WHERE `id` = \'%d\'',
			$this->db->e($name),
			static::$table,
			$this->id
		))[$name] ?? null;
	}
	/**
	 * Update multiple values
	 * @param array $_data
	 */
	function update(array $_data) {
		$data = [];
		foreach($_data as $key => $value)
			$data[] = sprintf(
				'`%s` = \'%s\'',
				(string)$this->db->e($key),
				(string)$this->db->e($value)
			);
		$this->db->query(sprintf(
			'UPDATE `%s` SET %s WHERE `id` = \'%d\'',
			static::$table,
			join(', ', $data),
			$this->id
		));
	}
	/**
	 * Set one value as object value
	 * @param mixed $name
	 * @param mixed $value
	 */
	function __set($name, $value) {
		$this->update([
			$name => $value
		]);
	}
	/**
	 * Search matching rows and make objects from them
	 * @param  string $query
	 * @param  array  $variables
	 * @return array
	 */
	static function find(string $query, array $variables=[]) {
		$rows = DB::getDefault()->select(sprintf(
			'SELECT `id` FROM `%s` %s',
			static::$table,
			sprintf($query, ...array_map(function($x){
				return DB::getDefault()->e($x);
			}, $variables))
		));
		return array_filter(array_map(function($x) {
			if(is_array($x) && count($x) > 0)
				return new static($x['id']);
			return false;
		}, $rows), function($x) {
			return $x !== false;
		}) ?? [];
	}
	/**
	 * Get first row object or false
	 * @param  string $query
	 * @param  array  $variables
	 * @return static|false
	 */
	static function first(string $query, array $variables=[]) {
		return static::find($query, $variables)[0] ?? false;
	}
}