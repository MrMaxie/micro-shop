<?php
namespace MicroShop;

use \MicroShop\{Singleton, DI};

class Router extends Singleton {
	static $instance;
	/**
	 * Error route
	 * @var callable
	 */
	protected $error;
	/**
	 * Storage of all aviable routes
	 * @var array
	 */
	protected $routes = [];
	/**
	 * Create new route
	 * @param string   $name
	 * @param string   $regexp
	 * @param callable $callable
	 */
	function route(string $name, string $regexp, $callable) {
		$this->routes[$name] = [
			'pattern'  => $regexp,
			'callable' => $callable
		];
	}
	/**
	 * Set-up error route
	 * @param callable $callable
	 */
	function notFound($callable) {
		$this->error = $callable;
	}
	/**
	 * Execute given route
	 * @param string|null $url
	 */
	function exec(string $url=null) {
		$url = $url ?? $_SERVER['REQUEST_URI'];
		$found = false;
		foreach($this->routes as $route) {
			if(preg_match($route['pattern'], $url, $m)) {
				$found = true;
				if(is_array($route['callable'])) {
					$controller_name = $route['callable'][0];
					$controller = new $controller_name();
					$callable = \Closure::fromCallable([$controller, $route['callable'][1]]);
				} else
					$callable = $route['callable'];
				$res = DI::getDefault()->exec($callable, $m);
				if($res !== false && $res !== '404') {
					break;
				} elseif($res === '404') {
					$found = false;
					break;
				}
			}
		}
		if(!$found && isset($this->error)) {
			if(is_array($this->error)) {
				$controller_name = $this->error[0];
				$controller = new $controller_name();
				$callable = \Closure::fromCallable([$controller, $this->error[1]]);
			} else
				$callable = $this->error;
			call_user_func($callable, $url); 
		}
	}
	/**
	 * Change header location
	 * @param string $location
	 */
	function location(string $location='/') {
		header("Location: $location");
		die;
	}
}