<?php
namespace MicroShop;

abstract class Singleton {
	/**
	 * Default instance
	 * Must be created in all singletons 
	 * @var static 
	 */
	// static $instance;
	/**
	 * Set-up this object if should be set a default one
	 */
	function __construct() {
		if(!isset($this::$instance))
			$this->setDefault();		
	}
	/**
	 * Set this object as default
	 */
	function setDefault() {
		$this::$instance = $this;
	}
	/**
	 * Return saved instance
	 * @return Router
	 */
	static function getDefault() {
		if(!isset(static::$instance))
			static::$instance = new static;
		return static::$instance;
	}
}