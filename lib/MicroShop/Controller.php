<?php
namespace MicroShop;

use \MicroShop\{View, Router};

class Controller {
	/**
	 * @var View
	 */
	protected $view;
	/**
	 * @var Router
	 */
	protected $router;
	/**
	 * Set-up View and Router object
	 */
	function __construct() {
		$this->view   = View::getDefault();
		$this->router = Router::getDefault();
	}
	/**
	 * Check if its POST request
	 * @return boolean
	 */
	function isPost() {
		return $_SERVER['REQUEST_METHOD'] === 'POST';
	}
}