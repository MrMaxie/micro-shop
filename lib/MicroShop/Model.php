<?php
namespace MicroShop;

class Model {
	/**
	 * Database instance
	 * @var DB
	 */
	protected $db;
	/**
	 * Set database as local
	 */
	function __construct() {
		$this->db = DB::getDefault();
	}
}