<?php
namespace MicroShop;

use \MicroShop\Singleton;

class DI extends Singleton {
	static $instance;
	/**
	 * @var array
	 */
	protected $data = [];
	/**
	 * @param array $values
	 */
	function __construct(array $values=[]) {
		parent::__construct();
		$this->setM($values);
	}
	/**
	 * Set single value
	 * @param string $name
	 * @param mixed  $value
	 */
	function set(string $name, $value) {
		$this->data[$name] = $value;
	}
	/**
	 * Normalize name
	 * @param  string $name
	 * @return string|false
	 */
	static function normalizeName(string $name) {
		$name = trim($name);
		$name = preg_replace('/[^a-zA-Z0-9_]/i', '', $name);
		if(strlen($name) < 1) return false;
		if(preg_match('/^(\d+|_)$/', $name))
			$name = '_'.(string)$name;
		return $name;
	}
	/**
	 * Set multiple values
	 * @param array $values
	 */
	function setM(array $values) {
		foreach($values as $name => $value)
			$this->set($name, $value);
	}
	/**
	 * Execute given callable with collected data
	 * @param  callable $callable
	 * @param  array    $data
	 * @return mixed
	 */
	function exec(callable $callable, array $tempData=[]) {
		$ref = new \ReflectionFunction($callable);
		$data = [];
		foreach(array_merge($this->data, $tempData) as $name => $value) {
			$name = static::normalizeName($name);
			if($name === false) continue;
			$data[$name] = $value;
		}
		return call_user_func_array($callable, array_map(function($x) use ($data) {
			return $data[$x->getName()] ?? ($x->isDefaultValueAvailable() ? $x->getDefaultValue() : null);
		}, $ref->getParameters()));
	}
}