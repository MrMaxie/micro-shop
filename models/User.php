<?php
use \MicroShop\ORM;

class User extends ORM {
	static $table = 'user';
	/**
	 * Logout logged user
	 */
	static function logout() {
		unset($_SESSION['logged_user_id']);
	}
	/**
	 * Try to register given user
	 * @param  string $login    [description]
	 * @param  string $password [description]
	 * @param  string $mail     [description]
	 * @return string
	 */
	static function register(string $login, string $password, string $mail): string {
		if(count(static::find('WHERE `login` = \'%s\'',[$login])) > 0)
			return 'login';
		if(count(static::find('WHERE `mail` = \'%s\'',[$mail])) > 0)
			return 'mail';
		new User([
			'login'    => $login,
			'password' => static::hashPassword($password),
			'mail'     => $mail
		]);
		if(count(static::find('WHERE `login` = \'%s\'',[$login])) > 0)
			return 'ok';
		return '?';
	}
	/**
	 * Try to login user using given login and password
	 * @param  string $login
	 * @param  string $password
	 * @param  bool   $remember
	 */
	static function login(string $login, string $password, bool $remember) {
		if(strlen($login) > 64) return false;
		$user = static::first('WHERE `login` = \'%s\' AND `password` = \'%s\'',
			[$login, static::hashPassword($password)]);
		if($user instanceof User) {
			$user->setAsLogged();
			return true;
		}
		return false;
	}
	/**
	 * Login this user
	 */
	function setAsLogged() {
		$_SESSION['logged_user_id'] = $this->id;	
	}
	/**
	 * Get logged here user
	 * @return static|false
	 */
	static function getLogged() {
		if(isset($_SESSION['logged_user_id']))
			return new static($_SESSION['logged_user_id']);
		return false;
	}
	/**
	 * Check if some user is logged
	 * @return boolean
	 */
	static function isLogged() {
		return static::getLogged() !== false;
	}
	/**
	 * Hash user password
	 * @param  string $password
	 * @return string Hashed one
	 */
	static function hashPassword(string $password): string {
		return hash('sha256', '~!_x'.$password.'XX__4GPBT.__12');
	}
}