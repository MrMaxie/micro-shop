<?php
use \MicroShop\{Captcha, Controller};

class cUser extends Controller {
	function login() {
		if($this->isPost()) {
			if(User::login(
				$_POST['login']    ?? '',
				$_POST['password'] ?? '',
				(bool)($_POST['remember'] ?? false)
			)) {
				$this->router->location('/');
			} else {
				$this->view->pushError('Podana kombinacja loginu i hasła jest nieprawidłowa');
			}
			$this->view->set('form_login', $_POST['login'] ?? '');
			$this->view->set('form_remember', (bool)($_POST['remember'] ?? false));
		}
		echo $this->view->render(['base', 'login']);
	}
	function register() {
		if($this->isPost()) {
			$l  = trim($_POST['login'])      ?? '';
			$m  = trim($_POST['mail'])       ?? '';
			$p  = trim($_POST['password'])   ?? '';
			$rp = trim($_POST['repassword']) ?? '';
			do {
				if(!Captcha::test($_POST['captcha'] ?? '')) {
					$this->view->pushError('Podany kod Captcha jest nieprawidłowy');
					break;
				}
				if(strlen($l) < 4
				|| strlen($l) > 64) {
					$this->view->pushError('Podane login jest za krótki,<br>musi zawierać minimum 6 znaków');
					break;
				}
				if(strlen($p) < 6) {
					$this->view->pushError('Podane hasło jest za krótkie,<br>musi zawierać minimum 6 znaków');
					break;
				}
				if(!filter_var($m, FILTER_VALIDATE_EMAIL)
				|| strlen($m) > 254) {
					$this->view->pushError('Podany adres e-mail nie wygląda prawidłowo');
					break;
				}
				if($p !== $rp) {
					$this->view->pushError('Podane hasła są inne');
					break;
				}
				$e = User::register($l,$p,$m);
				if($e === 'ok') {
					$this->view->pushSuccess('Użytkownik <strong>'.$l.'</strong> został poprawnie zarejestrowany');
					$this->router->location('/login');
				} else {
					$this->view->pushError([
						'mail'  => 'Podany adres e-mail jest już zajęty',
						'login' => 'Podany login jest już zajęty'
					][$e] ?? 'Wystąpił nieznany błąd podczas rejestracji,<br>skontaktuj się z administratorem');
				}
			} while(false);
			$this->view->set('form_login', $l);
			$this->view->set('form_mail',  $m);
		}
		echo $this->view->render(['base', 'register']);
	}
	function logout() {
		User::logout();
		$this->router->location('/');
	}
}