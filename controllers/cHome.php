<?php
use \MicroShop\Controller;

class cHome extends Controller {
	function __construct() {
		parent::__construct();
	}
	function default() {
		$this->view->set('page', 'home');
		echo $this->view->render(['base']);
	}
	function e404() {
		echo $this->view->render(['base', '404']);
	}
	function captcha() {
		$c = new \MicroShop\Captcha();
		$c->print();
	}
}